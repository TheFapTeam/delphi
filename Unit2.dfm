object Nagruzka: TNagruzka
  Left = 0
  Top = 0
  Caption = #1053#1072#1075#1088#1091#1079#1082#1072
  ClientHeight = 305
  ClientWidth = 1164
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 8
    Top = 8
    Width = 1017
    Height = 233
    DataSource = ds1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'NAGR_ID'
        Title.Caption = #1053#1086#1084#1077#1088' '#1085#1072#1075#1088#1091#1079#1082#1080
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAGR_YEAR'
        Title.Caption = #1059#1095#1077#1073#1085#1099#1081' '#1075#1086#1076
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAGR_LECT'
        Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1086#1074' '#1083#1077#1082#1094#1080#1081' ('#1095#1072#1089')'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAGR_PRACT'
        Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1087#1088#1072#1082#1090#1080#1082#1080' ('#1095#1072#1089')'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAGR_FORM'
        Title.Caption = #1060#1086#1088#1084#1072' '#1086#1090#1095#1077#1090#1085#1086#1089#1090#1080
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GR_ID'
        Title.Caption = #1053#1086#1084#1077#1088' '#1075#1088#1091#1087#1087#1099
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PREP_ID'
        Title.Caption = #1055#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DISC_ID'
        Title.Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1072
        Visible = True
      end>
  end
  object MainMenu1: TMainMenu
    Left = 456
    Top = 248
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1042#1099#1093#1086#1076
    end
  end
  object qry1: TADOQuery
    Active = True
    Connection = Form1.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from nagruzka')
    Left = 24
    Top = 256
  end
  object ds1: TDataSource
    DataSet = qry1
    Left = 88
    Top = 256
  end
end
