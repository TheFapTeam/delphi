object NagruzkaEdit: TNagruzkaEdit
  Left = 0
  Top = 0
  Caption = #1053#1072#1075#1088#1091#1079#1082#1072': '#1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1079#1072#1087#1080#1089#1080
  ClientHeight = 389
  ClientWidth = 419
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 105
    Top = 27
    Width = 113
    Height = 21
    Caption = #1053#1086#1084#1077#1088' '#1079#1072#1087#1080#1089#1080
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 190
    Top = 67
    Width = 28
    Height = 21
    Caption = #1043#1086#1076
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 158
    Top = 107
    Width = 60
    Height = 21
    Caption = #1051#1077#1082#1094#1080#1080
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 143
    Top = 147
    Width = 75
    Height = 21
    Caption = #1055#1088#1072#1082#1090#1080#1082#1072
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 105
    Top = 227
    Width = 118
    Height = 21
    Caption = #1053#1086#1084#1077#1088' '#1075#1088#1091#1087#1087#1099
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 105
    Top = 267
    Width = 119
    Height = 21
    Caption = #1055#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1100
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 119
    Top = 307
    Width = 99
    Height = 21
    Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1072
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 68
    Top = 187
    Width = 150
    Height = 21
    Caption = #1060#1086#1088#1084#1072' '#1086#1090#1095#1077#1090#1085#1086#1089#1090#1080
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 232
    Top = 29
    Width = 145
    Height = 21
    Enabled = False
    TabOrder = 0
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 232
    Top = 69
    Width = 145
    Height = 21
    TabOrder = 1
  end
  object Edit3: TEdit
    Left = 232
    Top = 109
    Width = 145
    Height = 21
    TabOrder = 2
  end
  object Edit4: TEdit
    Left = 232
    Top = 149
    Width = 145
    Height = 21
    TabOrder = 3
  end
  object DBComboBox1: TDBComboBox
    Left = 232
    Top = 229
    Width = 145
    Height = 21
    DataField = 'GR_ID'
    DataSource = DataSource1
    TabOrder = 4
  end
  object DBComboBox2: TDBComboBox
    Left = 232
    Top = 269
    Width = 145
    Height = 21
    DataField = 'PREP_FAM'
    DataSource = DataSource2
    TabOrder = 5
  end
  object DBComboBox3: TDBComboBox
    Left = 232
    Top = 309
    Width = 145
    Height = 21
    DataField = 'DISC_NAME'
    DataSource = DataSource3
    TabOrder = 6
  end
  object Edit5: TEdit
    Left = 232
    Top = 189
    Width = 145
    Height = 21
    TabOrder = 7
  end
  object Button1: TButton
    Left = 232
    Top = 336
    Width = 145
    Height = 33
    Caption = 'Button1'
    TabOrder = 8
  end
  object Button2: TButton
    Left = 383
    Top = 227
    Width = 26
    Height = 25
    Caption = '+'
    TabOrder = 9
  end
  object Button3: TButton
    Left = 383
    Top = 267
    Width = 26
    Height = 25
    Caption = '+'
    TabOrder = 10
  end
  object Button4: TButton
    Left = 383
    Top = 307
    Width = 26
    Height = 25
    Caption = '+'
    TabOrder = 11
  end
  object qry1: TADOQuery
    Connection = Form1.con1
    Parameters = <>
    SQL.Strings = (
      'select NAGR_ID from nagruzka')
    Left = 16
    Top = 344
  end
  object qry2: TADOQuery
    Parameters = <>
    Left = 64
    Top = 344
  end
  object qry3: TADOQuery
    Connection = Form1.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from gruppa')
    Left = 480
    Top = 224
  end
  object DataSource1: TDataSource
    DataSet = qry3
    Left = 456
    Top = 224
  end
  object qry4: TADOQuery
    Connection = Form1.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from prepodavatel')
    Left = 480
    Top = 264
  end
  object qry5: TADOQuery
    Connection = Form1.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from disciplina')
    Left = 480
    Top = 296
  end
  object DataSource2: TDataSource
    DataSet = qry4
    Left = 448
    Top = 264
  end
  object DataSource3: TDataSource
    DataSet = qry5
    Left = 448
    Top = 296
  end
end
