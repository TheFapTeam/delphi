object GruppaEdit: TGruppaEdit
  Left = 0
  Top = 0
  Caption = #1043#1088#1091#1087#1087#1072': '#1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1079#1072#1087#1080#1089#1080
  ClientHeight = 377
  ClientWidth = 506
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 105
    Top = 27
    Width = 120
    Height = 21
    Caption = #1053#1086#1084#1077#1088' '#1043#1088#1091#1087#1087#1099
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 186
    Top = 67
    Width = 40
    Height = 21
    Caption = #1050#1091#1088#1089
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 91
    Top = 107
    Width = 135
    Height = 21
    Caption = #1043#1086#1076' '#1087#1086#1089#1090#1091#1087#1083#1077#1085#1080#1103
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 46
    Top = 147
    Width = 180
    Height = 21
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1089#1090#1091#1076#1077#1085#1090#1086#1074
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 103
    Top = 187
    Width = 123
    Height = 21
    Caption = #1057#1087#1077#1094#1080#1072#1083#1100#1085#1086#1089#1090#1100
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 232
    Top = 29
    Width = 145
    Height = 21
    Enabled = False
    TabOrder = 0
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 232
    Top = 69
    Width = 145
    Height = 21
    TabOrder = 1
  end
  object Edit3: TEdit
    Left = 232
    Top = 109
    Width = 145
    Height = 21
    TabOrder = 2
  end
  object Edit4: TEdit
    Left = 232
    Top = 149
    Width = 145
    Height = 21
    TabOrder = 3
  end
  object Button1: TButton
    Left = 232
    Top = 264
    Width = 145
    Height = 33
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 383
    Top = 187
    Width = 26
    Height = 25
    Caption = '+'
    TabOrder = 5
  end
  object ComboBox1: TComboBox
    Left = 232
    Top = 189
    Width = 145
    Height = 21
    TabOrder = 6
  end
  object qry1: TADOQuery
    Connection = Form1.con1
    Parameters = <>
    SQL.Strings = (
      'select GR_ID from gruppa')
    Left = 48
    Top = 320
  end
  object qry2: TADOQuery
    Parameters = <>
    Left = 96
    Top = 320
  end
  object qry3: TADOQuery
    Connection = Form1.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from specialnost')
    Left = 478
    Top = 184
  end
  object DataSource1: TDataSource
    DataSet = qry3
    Left = 432
    Top = 184
  end
  object DataSource2: TDataSource
    DataSet = ADOQuery1
    Left = 432
    Top = 240
  end
  object ADOQuery1: TADOQuery
    Connection = Form1.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select* from gruppa')
    Left = 472
    Top = 240
  end
end
