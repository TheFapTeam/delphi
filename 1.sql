-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.25 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.1.0.4556
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных works
CREATE DATABASE IF NOT EXISTS `works` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `works`;


-- Дамп структуры для таблица works.access
CREATE TABLE IF NOT EXISTS `access` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Login` varchar(50) NOT NULL DEFAULT '0',
  `Password` varchar(50) NOT NULL DEFAULT '0',
  `Access_type_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FK__access_type` (`Access_type_ID`),
  CONSTRAINT `FK__access_type` FOREIGN KEY (`Access_type_ID`) REFERENCES `access_type` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы works.access: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `access` DISABLE KEYS */;
/*!40000 ALTER TABLE `access` ENABLE KEYS */;


-- Дамп структуры для таблица works.access_type
CREATE TABLE IF NOT EXISTS `access_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы works.access_type: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `access_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_type` ENABLE KEYS */;


-- Дамп структуры для таблица works.disciplina
CREATE TABLE IF NOT EXISTS `disciplina` (
  `DISC_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISC_NAME` varchar(20) NOT NULL,
  PRIMARY KEY (`DISC_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы works.disciplina: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `disciplina` DISABLE KEYS */;
INSERT INTO `disciplina` (`DISC_ID`, `DISC_NAME`) VALUES
	(1, 'Информатика'),
	(2, 'Математический анали'),
	(3, 'Линейная алгебра');
/*!40000 ALTER TABLE `disciplina` ENABLE KEYS */;


-- Дамп структуры для таблица works.dolgnost
CREATE TABLE IF NOT EXISTS `dolgnost` (
  `DOLG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOLG_NAME` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`DOLG_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы works.dolgnost: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `dolgnost` DISABLE KEYS */;
INSERT INTO `dolgnost` (`DOLG_ID`, `DOLG_NAME`) VALUES
	(1, 'Ассистент'),
	(2, 'Заведующий'),
	(3, 'Доцент'),
	(4, 'Старший преподавтель');
/*!40000 ALTER TABLE `dolgnost` ENABLE KEYS */;


-- Дамп структуры для таблица works.facultet
CREATE TABLE IF NOT EXISTS `facultet` (
  `FAC_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FAC_NAME` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`FAC_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы works.facultet: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `facultet` DISABLE KEYS */;
INSERT INTO `facultet` (`FAC_ID`, `FAC_NAME`) VALUES
	(1, 'Механико-математический');
/*!40000 ALTER TABLE `facultet` ENABLE KEYS */;


-- Дамп структуры для таблица works.gruppa
CREATE TABLE IF NOT EXISTS `gruppa` (
  `GR_ID` varchar(50) NOT NULL,
  `GR_COURSE` int(11) DEFAULT NULL,
  `GR_YEAR` year(4) DEFAULT NULL,
  `GR_COUNT` int(11) DEFAULT NULL,
  `SPEC_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`GR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы works.gruppa: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `gruppa` DISABLE KEYS */;
INSERT INTO `gruppa` (`GR_ID`, `GR_COURSE`, `GR_YEAR`, `GR_COUNT`, `SPEC_ID`) VALUES
	('10501.10', 3, '2012', 22, 1),
	('10502.10', 4, '2011', 25, 1);
/*!40000 ALTER TABLE `gruppa` ENABLE KEYS */;


-- Дамп структуры для таблица works.journal
CREATE TABLE IF NOT EXISTS `journal` (
  `ID_REC` int(11) NOT NULL AUTO_INCREMENT,
  `ID_TEACHER` text,
  `DATE` date DEFAULT NULL,
  `ID_LABEL_TYPE` text,
  PRIMARY KEY (`ID_REC`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы works.journal: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `journal` DISABLE KEYS */;
INSERT INTO `journal` (`ID_REC`, `ID_TEACHER`, `DATE`, `ID_LABEL_TYPE`) VALUES
	(1, '1', '2014-10-14', '12'),
	(2, '1', '2014-09-10', '12');
/*!40000 ALTER TABLE `journal` ENABLE KEYS */;


-- Дамп структуры для таблица works.kafedra
CREATE TABLE IF NOT EXISTS `kafedra` (
  `CAF_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CAF_NAME` varchar(50) DEFAULT NULL,
  `Short_Name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`CAF_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы works.kafedra: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `kafedra` DISABLE KEYS */;
INSERT INTO `kafedra` (`CAF_ID`, `CAF_NAME`, `Short_Name`) VALUES
	(1, 'Информатики и вычислительной математики', 'ИиВМ');
/*!40000 ALTER TABLE `kafedra` ENABLE KEYS */;


-- Дамп структуры для таблица works.label_type
CREATE TABLE IF NOT EXISTS `label_type` (
  `ID_LABEL_TYPE` int(11) NOT NULL AUTO_INCREMENT,
  `NAME_LABEL_TYPE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID_LABEL_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы works.label_type: ~12 rows (приблизительно)
/*!40000 ALTER TABLE `label_type` DISABLE KEYS */;
INSERT INTO `label_type` (`ID_LABEL_TYPE`, `NAME_LABEL_TYPE`) VALUES
	(1, 'Выходные'),
	(2, 'Работа в ночное время'),
	(3, 'Нетрудоспособность'),
	(4, 'Очередной отпуск'),
	(5, 'Декретный отпуск'),
	(6, 'Прогул'),
	(7, 'Командировки'),
	(8, 'Административный отпуск'),
	(9, 'Отпуск по учебе'),
	(10, 'Отпуск по уходу за ребенком'),
	(11, 'Работа в праздники'),
	(12, 'Явка');
/*!40000 ALTER TABLE `label_type` ENABLE KEYS */;


-- Дамп структуры для таблица works.nagruzka
CREATE TABLE IF NOT EXISTS `nagruzka` (
  `NAGR_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAGR_YEAR` year(4) DEFAULT NULL,
  `NAGR_LECT` int(11) DEFAULT NULL,
  `NAGR_PRACT` int(11) DEFAULT NULL,
  `NAGR_FORM` varchar(20) DEFAULT NULL,
  `GR_ID` varchar(50) DEFAULT NULL,
  `PREP_ID` varchar(50) DEFAULT NULL,
  `DISC_ID` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`NAGR_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы works.nagruzka: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `nagruzka` DISABLE KEYS */;
INSERT INTO `nagruzka` (`NAGR_ID`, `NAGR_YEAR`, `NAGR_LECT`, `NAGR_PRACT`, `NAGR_FORM`, `GR_ID`, `PREP_ID`, `DISC_ID`) VALUES
	(1, '2014', 50, 80, 'Экзамен', '10501.10', 'Иванов И.И.', 'Информатика'),
	(2, '2014', 60, 70, 'Экзамен', '10501.10', 'Иванов И.И.', 'Математический анализ'),
	(3, '2014', 70, 60, 'Экзамен', '10502.10', 'Петров П.П.', 'Линейная алгебра'),
	(4, '2014', 80, 50, 'Экзамен', '10502.10', 'Иванов И.И.', 'Математический анализ');
/*!40000 ALTER TABLE `nagruzka` ENABLE KEYS */;


-- Дамп структуры для таблица works.prepodavatel
CREATE TABLE IF NOT EXISTS `prepodavatel` (
  `PREP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PREP_FAM` varchar(50) DEFAULT NULL,
  `PREP_NAME` varchar(50) DEFAULT NULL,
  `PREP_OTCH` varchar(50) DEFAULT NULL,
  `PREP_PHONE` varchar(50) DEFAULT NULL,
  `PREP_MAIL` varchar(50) DEFAULT NULL,
  `DOLG_ID` int(11) DEFAULT NULL,
  `CAF_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PREP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы works.prepodavatel: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `prepodavatel` DISABLE KEYS */;
INSERT INTO `prepodavatel` (`PREP_ID`, `PREP_FAM`, `PREP_NAME`, `PREP_OTCH`, `PREP_PHONE`, `PREP_MAIL`, `DOLG_ID`, `CAF_ID`) VALUES
	(1, 'Иванов', 'Иван', 'Иванович', '111-11-11', 'ivanov@iv.ru', 3, 1),
	(2, 'Петров', 'Петр', 'Петрович', '222-22-22', 'petrov@pe.ru', 1, 1),
	(3, 'Степанов', 'Анатолий', 'Николаевич', '235-63-68', 'stepanob@gmail.com', 2, 1);
/*!40000 ALTER TABLE `prepodavatel` ENABLE KEYS */;


-- Дамп структуры для таблица works.specialnost
CREATE TABLE IF NOT EXISTS `specialnost` (
  `SPEC_ID` int(11) NOT NULL,
  `SPEC_NAME` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`SPEC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы works.specialnost: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `specialnost` DISABLE KEYS */;
INSERT INTO `specialnost` (`SPEC_ID`, `SPEC_NAME`) VALUES
	(1, 'Математическое обеспечение и администрирвоание информационных систем');
/*!40000 ALTER TABLE `specialnost` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
