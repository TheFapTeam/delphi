object SpecialnostEdit: TSpecialnostEdit
  Left = 0
  Top = 0
  Caption = #1057#1087#1077#1094#1080#1072#1083#1100#1085#1086#1089#1090#1100': '#1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1079#1072#1087#1080#1089#1080
  ClientHeight = 171
  ClientWidth = 416
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 48
    Top = 27
    Width = 178
    Height = 21
    Caption = #1053#1086#1084#1077#1088' '#1089#1087#1077#1094#1080#1072#1083#1100#1085#1086#1089#1090#1080
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 110
    Top = 67
    Width = 116
    Height = 21
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 232
    Top = 29
    Width = 145
    Height = 21
    Enabled = False
    TabOrder = 0
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 232
    Top = 69
    Width = 145
    Height = 21
    TabOrder = 1
  end
  object Button1: TButton
    Left = 232
    Top = 112
    Width = 145
    Height = 33
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 2
    OnClick = Button1Click
  end
  object qry1: TADOQuery
    Connection = Form1.con1
    Parameters = <>
    SQL.Strings = (
      'select SPEC_ID from specialnost')
    Left = 72
    Top = 111
  end
  object qry2: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'select GR_ID from specialnost')
    Left = 120
    Top = 111
  end
  object qry3: TADOQuery
    Connection = Form1.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from gruppa')
    Left = 478
    Top = 184
  end
  object DataSource1: TDataSource
    DataSet = qry3
    Left = 456
    Top = 184
  end
end
