object gruppa: Tgruppa
  Left = 0
  Top = 0
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082': '#1043#1088#1091#1087#1087#1072
  ClientHeight = 279
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 8
    Top = 8
    Width = 619
    Height = 233
    DataSource = ds1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'GR_ID'
        Title.Caption = #1053#1086#1084#1077#1088' '#1075#1088#1091#1087#1087#1099
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GR_COURSE'
        Title.Caption = #1050#1091#1088#1089
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GR_YEAR'
        Title.Caption = #1043#1086#1076' '#1087#1086#1089#1090#1091#1087#1083#1077#1085#1080#1103
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GR_COUNT'
        Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1089#1090#1091#1076#1077#1085#1090#1086#1074
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SPEC_ID'
        Title.Caption = #1053#1086#1084#1077#1088' '#1089#1087#1077#1094#1080#1072#1083#1100#1085#1086#1089#1090#1080
        Visible = True
      end>
  end
  object MainMenu1: TMainMenu
    Left = 456
    Top = 248
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1042#1099#1093#1086#1076
    end
  end
  object qry1: TADOQuery
    Active = True
    Connection = Form1.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from gruppa')
    Left = 24
    Top = 251
  end
  object ds1: TDataSource
    DataSet = qry1
    Left = 88
    Top = 251
  end
end
